//! Worker application.
//!
//! ---
//! author: Andrew Evans
//! ---
use std::collections::HashMap;
use std::sync::Arc;

use datacannon_rs_core::config::config::CannonConfig;
use datacannon_rs_worker::app::app::{App, AppBuilder};
use datacannon_rs_worker::task::wrapper::{FutureTask, Task};
use tokio::sync::{Mutex, Notify, RwLock};
use tokio::task::JoinHandle;

use crate::app::config::app_config::ETLAppConfig;

/// ETL Application
pub struct ETLApp{
    pub app: Arc<Mutex<App>>
}


impl ETLApp{

    /// Close the application
    pub async fn close(&mut self){
        let app = self.app.clone();
        let mut mgaurd = app.lock().await;
        mgaurd.close().await;
    }

    /// Register a custom task with the application
    ///
    /// # Arguments
    /// * `task_name` - Name of the task
    /// * `task` - Custom task
    pub async fn register_custom_task(
        &mut self,
        task_name: &'static str,
        task: Task){
        let mut mgaurd = self.app.lock().await;
        mgaurd.register_custom_task(task_name, task);
    }

    /// Register the cpu task.
    ///
    /// # Arguments
    /// * `task_name` - Name of the task
    /// * `task` - The task future
    pub async fn register_cpu_task(
        &mut self,
        task_name: &'static str,
        task: Task){
        let mut mgaurd = self.app.lock().await;
        mgaurd.register_cpu_task(task_name, task);
    }

    /// Register an io based task
    ///
    /// # Arguments
    /// * `task_name` - Name of the task
    /// * `task` - The Future task
    pub async fn register_io_task(
        &mut self,
        task_name: &'static str,
        task: FutureTask) {
        let mut mgaurd = self.app.lock().await;
        mgaurd.register_io_task(task_name, task).await;
    }

    /// Run the application
    pub async fn run(&mut self, notify: Arc<Notify>) -> JoinHandle<()>{
        let app = self.app.clone();
        let arc_notify = notify.clone();
        tokio::spawn(async move {
            let mut mgaurd = app.lock().await;
            mgaurd.start(
                Arc::new(RwLock::new(HashMap::new())), arc_notify).await;
        })
    }

    /// Create a new application
    ///
    /// # Arguments
    /// * `app_name` - Name of the application
    /// * `app_config` - Application configuration
    /// * `cannon_config` - Cannon configuration
    pub async fn new(
        app_name: String,
        app_config: ETLAppConfig,
        cannon_config: CannonConfig<'static>) -> ETLApp{
        let mut builder = AppBuilder::new(cannon_config.clone());
        builder.app_name(app_name);
        builder.routers(cannon_config.routers.clone());
        builder.consumers(app_config.consumer_types);
        builder.brokers(app_config.broker_types);
        builder.backends(app_config.backends);
        builder.pool_types(app_config.pool_types);
        let app = builder.build();
        ETLApp{
            app: Arc::new(Mutex::new(app))
        }
    }
}


#[cfg(test)]
pub mod tests{
    use std::env;
    use std::sync::Arc;

    use datacannon_rs_core::backend::config::BackendConfig;
    use datacannon_rs_core::backend::types::AvailableBackend;
    use datacannon_rs_core::config::config::{BackendType, BrokerType, CannonConfig};
    use datacannon_rs_core::connection::amqp::connection_inf::AMQPConnectionInf;
    use datacannon_rs_core::connection::connection::ConnectionConfig;
    use datacannon_rs_core::message_structure::amqp::exchange::Exchange;
    use datacannon_rs_core::message_structure::amqp::queue::AMQPQueue;
    use datacannon_rs_core::message_structure::queues::GenericQueue;
    use datacannon_rs_core::replication::rabbitmq::{RabbitMQHAPolicies, RabbitMQHAPolicy};
    use datacannon_rs_core::replication::replication::HAPolicy;
    use datacannon_rs_core::router::router::{Router, Routers};
    use datacannon_rs_worker::consumer::consumer_types::ConsumerType;
    use datacannon_rs_worker::threadpool::cpu_pool::CPUPoolConfig;
    use datacannon_rs_worker::threadpool::pool_types::PoolType;
    use lapin::ExchangeKind;
    use tokio::runtime::{Builder, Runtime};
    use tokio::sync::Notify;

    use crate::app::config::app_config::ETLAppConfigBuilder;
    use crate::app::etl_app::ETLApp;

    /// Get the runtime.
    fn get_runtime() -> Runtime{
        let r = Builder::default()
            .threaded_scheduler()
            .core_threads(2)
            .enable_all()
            .build();
        r.unwrap()
    }

    fn get_routers() -> Routers {
        let mut rts = Routers::new();
        let mut queues = Vec::<GenericQueue>::new();
        let amq_conf = AMQPConnectionInf::new("amqp", "127.0.0.1", 5672, Some("test"), Some("dev"), Some("rtp*4500"), 1000);
        let q = AMQPQueue::new(
            "lapin_test_queue".to_string(),
            Some("test_exchange".to_string()),
            Some("test_route".to_string()),
            0,
            HAPolicy::RabbitMQ(RabbitMQHAPolicy::new(
                RabbitMQHAPolicies::ALL, 1)), false, amq_conf);
        queues.push(GenericQueue::AMQPQueue(q));
        let exch = Exchange::new("test_exchange", ExchangeKind::Direct);
        let router = Router::new("test_key".to_string(), queues, Some(exch));
        rts.add_router("test_key".to_string(), router);
        rts
    }

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp", "127.0.0.1",
            5672,
            Some("test"),
            Some(user.ok().unwrap().as_str()),
            Some(pwd.ok().unwrap().as_str()),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = get_backend_config();
        let routers = get_routers();
        let mut cannon_conf = CannonConfig::new(
            conn_conf,
            BackendType::REDIS,
            backend_conf,
            routers);
        cannon_conf.num_broker_channels = 30;
        cannon_conf.consumers_per_queue = 25;
        cannon_conf.default_routing_key = "test_key";
        cannon_conf.default_exchange = "test_exchange";
        cannon_conf.default_exchange_type = ExchangeKind::Direct;
        cannon_conf
    }

    fn get_backend_config() -> BackendConfig {
        let bc = BackendConfig {
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }

    fn get_cpu_pool_config() -> CPUPoolConfig{
        CPUPoolConfig{
            pool_size: 4,
            max_queued_tasks: 10,
            failures_per_n_calls: 10,
            reset_failures_on_n_calls: 100
        }
    }

    #[test]
    fn should_start_app(){
        let mut rt = get_runtime();
        rt.block_on(async move {
            let cfg = get_config();
            let pool_config = get_cpu_pool_config();
            let backend_config = get_backend_config();
            let mut builder = ETLAppConfigBuilder::default();
            builder.add_backend(AvailableBackend::Redis((backend_config, 2)));
            builder.add_consumer(ConsumerType::RabbitMQ);
            builder.add_broker(BrokerType::RABBITMQ);
            builder.add_pool_type(PoolType::CPUPOOL(pool_config));
            let app_config = builder.build();
            let mut app = ETLApp::new(
                "test_app".to_string(), app_config, cfg).await;
            let notify = Arc::new(Notify::new());
            let handle = app.run(notify.clone()).await;
            notify.notified().await;
            app.close().await;
            let _r = handle.await;
        });
    }
}