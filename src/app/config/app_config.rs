//! Application configuration.
//!
//! ---
//! author: Andrew Evans
//! ---

use datacannon_rs_worker::consumer::consumer_types::ConsumerType;
use datacannon_rs_core::config::config::BrokerType;
use datacannon_rs_core::backend::types::AvailableBackend;
use datacannon_rs_worker::threadpool::pool_types::PoolType;

/// Broker type configuration builder
pub struct ETLAppConfigBuilder{
    backends: Vec<AvailableBackend>,
    broker_types: Vec<BrokerType>,
    consumer_types: Vec<ConsumerType>,
    pool_types: Vec<PoolType>
}


impl ETLAppConfigBuilder{

    /// Extend the pool types for the application. Pool types must be unique.
    ///
    /// # Arguments
    /// * `pool_types` - Vector of pool types for the application
    pub fn extend_pool_types(&mut self, pool_types: Vec<PoolType>) -> &mut Self{
        self.pool_types.extend(pool_types);
        self
    }

    /// Set the pool types for the application. Pool types must be unique.
    ///
    /// # Arguments
    /// * `pool_types` - Vector of pool types
    pub fn set_pool_types(&mut self, pool_types: Vec<PoolType>) -> &mut Self{
        self.pool_types = pool_types;
        self
    }

    /// Add a pool type. Pool Types must be unique.
    ///
    /// # Arguments
    /// * `pool_type` - Pool type to add.
    pub fn add_pool_type(&mut self, pool_type: PoolType) -> &mut Self{
        self.pool_types.push(pool_type);
        self
    }

    /// Extend the backends vector for the application. Backends must be unique.
    ///
    /// # Arguments
    /// * `backends` - Vector of application backends
    pub fn extend_backends(&mut self, backends: Vec<AvailableBackend>) -> &mut Self{
        self.backends.extend(backends);
        self
    }

    /// Set the backends for the applications. Replaces the current vector.
    /// Backends must be unique.
    ///
    /// # Arguments
    /// * `backends` - Backends for the application
    pub fn set_backends(&mut self, backends: Vec<AvailableBackend>) -> &mut Self{
        self.backends = backends;
        self
    }

    /// Add a backend. Backends must be unique.
    ///
    /// # Arguments
    /// * `backend` - Backend to add to the application
    pub fn add_backend(&mut self, backend: AvailableBackend) -> &mut Self{
        self.backends.push(backend);
        self
    }

    /// Extend the brokers. Brokers must be unique.
    ///
    /// # Arguments
    /// * `brokers` - Vector of broker types
    pub fn extend_brokers(&mut self, brokers: Vec<BrokerType>) -> &mut Self {
        self.broker_types.extend(brokers);
        self
    }

    /// Set the brokers. Replaces existing broker vector. Brokers must be
    /// unique.
    ///
    /// # Arguments
    /// * `brokers` - Broker types vector
    pub fn set_brokers(&mut self, brokers: Vec<BrokerType>) -> &mut Self{
        self.broker_types = brokers;
        self
    }

    /// Add a broker type. Brokers must be unique.
    ///
    /// # Arguments
    /// * `broker` - The broker type
    pub fn add_broker(&mut self, broker: BrokerType) -> &mut Self{
        self.broker_types.push(broker);
        self
    }

    /// Extend the current consumers vector. Consumers must be unique.
    ///
    /// # Arguments
    /// * `consumers` - Vector of new consumers
    pub fn extend_consumers(&mut self, consumers: Vec<ConsumerType>) -> &mut Self{
        self.consumer_types.extend(consumers);
        self
    }

    /// Set the consumers. Replaces consumers. Consumers must be unique.
    ///
    /// # Arguments
    /// * `consumers` - Consumers types vector
    pub fn set_consumers(&mut self, consumers: Vec<ConsumerType>) -> &mut Self{
        self.consumer_types = consumers;
        self
    }

    /// Add a consumer type. Consumers must be unique.
    ///
    /// # Arguments
    /// * `consumer` - Type of consumer to create
    pub fn add_consumer(&mut self, consumer: ConsumerType) -> &mut Self{
        self.consumer_types.push(consumer);
        self
    }

    /// Get the builder with default values. You need to set at least one consumer type.
    pub fn default() -> ETLAppConfigBuilder{
        ETLAppConfigBuilder{
            backends: vec![],
            consumer_types: vec![],
            broker_types: vec![],
            pool_types: vec![]
        }
    }

    /// Build the configuration. Consumes the config.
    pub fn build(self) -> ETLAppConfig{
        ETLAppConfig{
            backends: self.backends,
            consumer_types: self.consumer_types,
            broker_types: self.broker_types,
            pool_types: self.pool_types
        }
    }
}


/// ETL Application configuration
pub struct ETLAppConfig {
    pub backends: Vec<AvailableBackend>,
    pub broker_types: Vec<BrokerType>,
    pub consumer_types: Vec<ConsumerType>,
    pub pool_types: Vec<PoolType>
}
