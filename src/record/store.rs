//! Stores records until they are emitted by the task. Packages records into
//! the response expected to be returned by each task.
//!
//! ---
//! author: Andrew Evans
//! ---

use crate::record::record_type::Record;
use datacannon_rs_core::task::result::Response;
use datacannon_rs_core::argparse::argtype::ArgType;

/// Record store
pub struct RecordStore{
    store: Vec<ArgType>
}


impl RecordStore{

    /// Package records to a response.
    pub fn package_records(&mut self) -> Response{
        let mut response = Response::new();
        let arg_map = ArgType::ArgVec(self.store.clone());
        response.add_result("records".to_string(), arg_map);
        response
    }

    /// Add to the store
    ///
    /// # Arguments
    /// * `record` - Record hashmap
    pub fn add_to_store(&mut self, record: Record){
        self.store.push(ArgType::ArgMap(record));
    }

    /// Create a new record store
    pub fn new() -> RecordStore{
        RecordStore{
            store: vec![]
        }
    }
}
