//! A record iterator to use in the tasks. Expects records to be in the specified
//! field. The default field is `records`. Creates an iterator and returns the
//! next record until empty.
//!
//! ---
//! author: Andrew Evans
//! ---

use datacannon_rs_core::task::context::TaskContext;
use datacannon_rs_core::argparse::argtype::ArgType;
use crate::record::record_type::Record;

/// A record iterator. Takes in the context.
pub struct RecordIterator{
    records: Vec<Record>
}


impl RecordIterator{

    /// Get the next record or None if empty.
    pub fn next(&mut self) -> Option<Record>{
        if self.records.is_empty() == false{
            let record_opt = self.records.pop();
            if record_opt.is_some(){
                let record = record_opt.unwrap();
                Some(record)
            }else{
                None
            }
        }else{
            None
        }
    }

    /// Check if there is a next record.
    pub fn has_next(&self) -> bool{
        !self.records.is_empty()
    }

    /// Creates a new record iterator from the `TaskContext`. Returns
    /// None if there are no records provided.
    ///
    /// # Arguments
    /// * `ctx` - Task context
    pub fn new(ctx: TaskContext) -> Option<RecordIterator>{
        let mut tctx = ctx.clone();
        let kwargs_opt = tctx.get_kwargs();
        if kwargs_opt.is_some(){
            let kwargs = kwargs_opt.unwrap();
            let records_opt = kwargs.get("records");
            if records_opt.is_some(){
                let mut record_it = vec![];
                let records= records_opt.unwrap().clone();
                if let ArgType::ArgVec(records) = records{
                    for record in records{
                        if let ArgType::ArgMap(record) = record{
                            record_it.push(record);
                        }
                    }
                }
                let rit = RecordIterator{
                    records: record_it
                };
                Some(rit)
            }else{
                None
            }
        }else{
            None
        }
    }
}
