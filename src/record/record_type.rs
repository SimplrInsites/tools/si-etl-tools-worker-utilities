//! A record type for storing information
//!
//! ---
//! author: Andrew Evans
//! ---

use std::collections::HashMap;
use datacannon_rs_core::argparse::argtype::ArgType;

pub type Record = HashMap<String, ArgType>;
