# SI ETL Tools Worker

The SI ETL tools is a low memory, fast, extendable, and simple system taking
care of the majority of work for distribution, batching, threading, and more.
We aim to provide access to a job queue capable of running ETL on anything
attached to PostgreSQL.

The worker runs tasks and receives batch jobs from the server. Datacannon
provides multiple threadpool types around futures and Tokio for true threading.

Workers use RabbitMQ or another broker to receive tasks, processing them on the
chosen threadpool. The worker sends responses back through the a distributed
queue such as Redis or handles chains and chords.

Look through the tests and any examples for an idea of how to setup the tool.
More later.

# Technologies through Datacannon

Inital support comes through Datacannon and includes support for:

- Rust
- Custom langauges using the jni or pyoe crates from Rust (java/python)
- RabbitMQ
- Redis

# Planned Support through Datacannon (longer term)

We hope to include broad support for:

- Kafka
- Actix-based broker
- MQTT

# Planned Capabilities

We plan to have at least the same functionality as Pentaho with additional 
support for GIS and NLP. The currently planned capabilities include:

- Named Entity Recognition
- Summarization
- String operations (split, combine, replace; etc.)
- Numerical operations
- regular expression matching
- Beautiful Soup support/HTML support
- Geocoding support through Mapbox w/ limit checking
- Geocoding support through another framework w/ limit checking
- Libpostal address standardization
- Database lookup and join
- Source combination
- HashMap Deduplication step (may be a bottleneck)
- Bloom Filter deduplication step (may be a bottlneck)
- Yugabyte Sink
- PostgreSQL Sink
- Yugabyte Source w/ PostGIS support
- PostgreSQL Source w/ PostGIS support
- File source
- CSV source
- Excel Source
- Statistical Deduplication step

These are not the only capabilities to be present in a final version of the tool
as more will be added later.

# Advantages

There are numerous advantages over frameworks such as Pentaho including:

- Fast out of the box with Rust and support for distributed systems built in 
- Batch support
- Redis backend support out of the box
- Chaining and chords to create workflows with ease
- Planned integration with even faster brokers 
- Flexible and low-code (not no-code)
- Extendable
- Low memory and CPU use by the underlying tool (you decide the rest)
- Graphical ETL workflow building

# Graphical Workflows

SI ETL tools achieves graphical workflows through chains and chords. Chains 
contain chords sent back to the broker for further execution. Chords are stream
components executed on the same system before the system sends a response. Since
Rust is truly threaded, it is ok to use chords. However, you will want to mix
chords and chains to avoid starving other tasks. Lean towards fairly short chords
with a larger set of chains then the reverse.

# Installation

The latest Rust 1.44 is required. Rust pacakges dependencies with each release
like a java jar. You need to run


# License
See the license file for more information